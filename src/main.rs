struct Initializing;
struct CreatingFolder;
struct CreatingFile;
struct CreatingLink;
struct Finished;

use std::{
    fs::create_dir,
    marker::PhantomData,
    ops::{Deref, Not},
    os::unix::prelude::ExitStatusExt,
    path::{Path, PathBuf},
    str::from_boxed_utf8_unchecked,
};

#[derive(Debug, Clone)]
struct Storage<'a, State> {
    location: &'a Path,
    folder_a: Option<&'a Path>,
    initialized: bool,
    _state: PhantomData<State>,
}

impl<'a, T> Drop for Storage<'a, T> {
    fn drop(&mut self) {
        println!("Droped")
    }
}

impl<'a> From<Storage<'a, Initializing>> for Storage<'a, CreatingFolder> {
    fn from(init: Storage<'a, Initializing>) -> Storage<'a, CreatingFolder> {
        Storage {
            location: init.location,
            folder_a: init.folder_a,
            initialized: init.initialized,
            _state: PhantomData,
        }
    }
}

impl<'a> Storage<'a, Initializing> {
    pub fn new() -> Storage<'a, Initializing> {
        let loc = Path::new("./tmp");
        Storage {
            location: loc,
            folder_a: None,
            initialized: false,
            _state: PhantomData,
        }
    }

    pub fn init(self) -> Result<Storage<'a, CreatingFolder>, String> {
        println!("Initialize");
        if !self.location.exists() {
            create_dir(self.location).unwrap();
            println!("Folder {} created", self.location.display());
        } else {
            println!("Already existed");
        }
        Ok(self.into())
    }
}

impl<'a> From<Storage<'a, CreatingFolder>> for Storage<'a, CreatingFile> {
    fn from(init: Storage<'a, CreatingFolder>) -> Storage<'a, CreatingFile> {
        Storage {
            location: init.location,
            folder_a: init.folder_a,
            initialized: init.initialized,
            _state: PhantomData,
        }
    }
}

impl<'a> Storage<'a, CreatingFolder> {
    pub fn create_folder(self) -> Result<Storage<'a, CreatingFile>, String> {
        let mut p = PathBuf::from(self.location);
        p.push("data");
        let folder = p.as_path();
        if !folder.exists() {
            create_dir(folder).unwrap();
            println!("Folder {} created", folder.display());
        } else {
            println!("Already existed: {}", folder.display());
        }
        Ok(self.into())
    }
}

impl<'a> From<Storage<'a, CreatingFile>> for Storage<'a, CreatingLink> {
    fn from(init: Storage<'a, CreatingFile>) -> Storage<'a, CreatingLink> {
        Storage {
            location: init.location,
            folder_a: init.folder_a,
            initialized: init.initialized,
            _state: PhantomData,
        }
    }
}

impl<'a> From<Storage<'a, CreatingFile>> for Storage<'a, Finished> {
    fn from(init: Storage<'a, CreatingFile>) -> Storage<'a, Finished> {
        Storage {
            location: init.location,
            folder_a: init.folder_a,
            initialized: init.initialized,
            _state: PhantomData,
        }
    }
}

impl<'a> Storage<'a, CreatingFile> {
    pub fn create_file(self) -> Result<Storage<'a, CreatingLink>, String> {
        println!("Create file");
        Ok(self.into())
    }

    pub fn finish(self) -> Result<Storage<'a, Finished>, String> {
        println!("Finish");
        Ok(self.into())
    }
}

impl<'a> From<Storage<'a, CreatingLink>> for Storage<'a, Finished> {
    fn from(init: Storage<'a, CreatingLink>) -> Storage<'a, Finished> {
        Storage {
            location: init.location,
            folder_a: init.folder_a,
            initialized: init.initialized,
            _state: PhantomData,
        }
    }
}

impl<'a> Storage<'a, CreatingLink> {
    pub fn create_link(self) -> Result<Storage<'a, Finished>, String> {
        println!("Create link");
        Ok(self.into())
    }

    pub fn finish(self) -> Result<Storage<'a, Finished>, String> {
        println!("Finish");
        Ok(self.into())
    }
}

impl<'a> Storage<'a, Finished> {
    pub fn done(self) -> () {
        println!("Done")
    }
}

fn main() {
    Storage::new()
        .init()
        .unwrap()
        .create_folder()
        .unwrap()
        .create_file()
        .unwrap()
        .finish()
        .unwrap()
        .done();
}
